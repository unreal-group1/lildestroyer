// Copyright Epic Games, Inc. All Rights Reserved.

#include "LilDestroy.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, LilDestroy, "LilDestroy" );
